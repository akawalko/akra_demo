<?php
namespace App\Tests\Unit;

use App\Model\Square;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SquareTest extends KernelTestCase
{
    /** @var ValidatorInterface */
    private $validator;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->validator = self::$container->get('validator');
    }

    public function testCreateSquareWithoutDimensions()
    {
        // create shape without giving side length
        $shape = new Square();
        $errors = $this->validator->validate($shape);
        $this->assertCount(1, $errors);

        /** @var $first ConstraintViolation */
        $first = $errors->get(0);

        $this->assertEquals('a', $first->getPropertyPath());
        $this->assertEquals(
            'This value should not be blank.',
            $first->getMessage()
        );
    }

    public function testCreateSquareWithInvalidDimensions()
    {
        // create shape with non numeric side length
        $shape = new Square('c');
        $errors = $this->validator->validate($shape);
        $this->assertCount(2, $errors);

        /** @var $first ConstraintViolation */
        $first = $errors->get(0);
        $this->assertEquals('a', $first->getPropertyPath());
        $this->assertEquals(
            'This value should be positive.',
            $first->getMessage()
        );

        /** @var $first ConstraintViolation */
        $second = $errors->get(1);
        $this->assertEquals('a', $second->getPropertyPath());
        $this->assertEquals(
            'This value should be of type numeric.',
            $second->getMessage()
        );
    }

    public function testCreateSquareWithPositiveSideLength()
    {
        $shape = new Square(4);
        $errors = $this->validator->validate($shape);
        $this->assertCount(0, $errors);
    }

    public function testCreateSquareAndCalculateAreaForZeroSideLength()
    {
        $shape = new Square(0);
        $this->assertEquals(0, $shape->area());
    }

    public function testCreateSquareAndCalculateAreaForPositiveSideLength()
    {
        $shape = new Square(3);
        $this->assertEquals(9, $shape->area());
    }

    public function testCreateSquareAndExportToArray()
    {
        $shape = new Square(5);
        $this->assertEquals(
            [
                'shape' => 'Square',
                'dimensions' => [
                    'a' => 5
                ]
            ],
            $shape->exportToArray()
        );
    }

    public function testCreateSquareAndExportToJson()
    {
        $shape = new Square(5);

        $this->assertEquals(
            '{"shape":"Square","dimensions":{"a":5}}',
            $shape->exportToJson()
        );
    }
}