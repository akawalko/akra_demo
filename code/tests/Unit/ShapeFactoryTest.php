<?php
namespace App\Tests\Unit;

use App\Factory\ShapeFactory;
use App\Model\Square;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShapeFactoryTest extends KernelTestCase
{
    /**
     * @var ShapeFactory
     */
    private $factory;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->factory = self::$container->get('App\Factory\ShapeFactory');
    }

    public function testNullAsJsonArgument()
    {
        $this->expectExceptionMessage('No data was given.');
        $this->factory->crateFromJson(null);
    }

    public function testEmptyStringAsJsonArgument()
    {
        $this->expectExceptionMessage('No data was given.');
        $this->factory->crateFromJson('');
    }

    public function testMalformedJsonAsJsonArgument()
    {
        $this->expectExceptionMessage('Malformed JSON was given.');
        $this->factory->crateFromJson('{"shape": "square", "dimensions": {"a": test}}');
    }

    public function testJsonWithUnsupportedShapeAsArgument()
    {
        $this->expectExceptionMessage('Unsupported shape was given.');
        $this->factory->crateFromJson('{"shape": "regularPolygon ", "dimensions": {"a": 3}}');
    }

    public function testJsonWithNoDimensionsAsJsonArgument()
    {
        $this->expectExceptionMessage('Dimensions were not specified.');
        $this->factory->crateFromJson('{"shape": "square"}');
    }

    public function testJsonWithEmptyDimensionsAsJsonArgument()
    {
        $this->expectExceptionMessage('Dimensions were not specified.');
        $this->factory->crateFromJson('{"shape": "square", "dimensions": {}}');
    }

    public function testJsonWithInvalidDimensionsAsJsonArgument()
    {
        $this->expectExceptionMessage('a: This value should be positive. a: This value should be of type numeric.');
        $this->factory->crateFromJson('{"shape": "square", "dimensions": {"a": "cebula"}}');
    }

    public function testJsonWithValidDimensionsAsJsonArgument()
    {
        $shape = $this->factory->crateFromJson('{"shape": "square", "dimensions": {"a": 5}}');
        $this->assertIsObject($shape);
        $this->assertInstanceOf(Square::class, $shape);
    }

    public function testArrayWithValidDimensionsAsArrayArgument()
    {
        $shapeSpecs = [
            'shape' => 'Square',
            'dimensions' => [
                'a' => 5
            ]
        ];
        $shape = $this->factory->createFromArray($shapeSpecs);
        $this->assertIsObject($shape);
        $this->assertInstanceOf(Square::class, $shape);
        $this->assertEquals($shapeSpecs, $shape->exportToArray());
    }
}
