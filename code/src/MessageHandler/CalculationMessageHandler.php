<?php
namespace App\MessageHandler;

use App\Entity\Calculation;
use App\Factory\ShapeFactory;
use App\Message\CalculationMessage;
use App\Repository\CalculationRepository;
use http\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Throwable;

class CalculationMessageHandler implements MessageHandlerInterface
{
    /** @var CalculationRepository */
    private $repository;

    /** @var ShapeFactory */
    private $shapeFactory;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(CalculationRepository $repository, ShapeFactory $shapeFactory, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->shapeFactory = $shapeFactory;
        $this->logger = $logger;
    }

    public function __invoke(CalculationMessage $message)
    {
        try {
            $calculation = $this->repository->find($message->getCalculationId());

            if ($calculation !== null
                && $calculation->getStatus() === Calculation::STATUS_NEW
            ) {
                $input = $calculation->getInput();
                $shape = $this->shapeFactory->createFromArray($input);
                $calculation
                    ->setOutput($shape->area())
                    ->setStatus(Calculation::STATUS_READY);
                $this->repository->update($calculation);
            }
        } catch (Throwable $e) {
            $this->logger->error($e, ['Calculation::' . $message->getCalculationId()]);

            if ($calculation !== null) {
                $calculation->setStatus(Calculation::STATUS_ERROR);
                $this->repository->update($calculation);
            }
        }
    }
}
