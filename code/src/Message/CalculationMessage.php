<?php
namespace App\Message;

class CalculationMessage
{
    /** @var int */
    private $calculationId;

    /**
     * CalculationMessage constructor.
     * @param int $calculationId
     */
    public function __construct(int $calculationId)
    {
        $this->calculationId = $calculationId;
    }

    /**
     * @return int
     */
    public function getCalculationId(): int
    {
        return $this->calculationId;
    }
}
