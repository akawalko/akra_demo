<?php
namespace App\Controller;

use App\Entity\Calculation;
use App\Factory\ShapeFactory;
use App\Message\CalculationMessage;
use App\Model\ShapeInterface;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

class CalculateController extends AbstractController
{
    const CODE_500_MSG = 'Something must have gone wrong.';

    /**
     * @Route("/calculate",
     *     name="calculate",
     *     methods={"POST"}
     * )
     */
    public function calculate(
        Request $request,
        ShapeFactory $shapeFactory,
        EntityManagerInterface $em,
        LoggerInterface $logger
    ): Response
    {
        try {
            /** @var ShapeInterface $shape */
            $shape = $shapeFactory->crateFromJson($request->getContent());
            /** @var Calculation $calculation */
            $calculation = $em->getRepository(Calculation::class)->saveNew($shape);
            $this->dispatchMessage(new CalculationMessage($calculation->getId()));

            return $this->json(['id' => $calculation->getId()], Response::HTTP_ACCEPTED);
        } catch (InvalidArgumentException $e) {
            return $this->json($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (Throwable $e) {
            $logger->error($e, (array) $request->getContent());
            return $this->json(self::CODE_500_MSG, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/calculation/{id}",
     *     name="calculation",
     *     methods={"GET"},
     *     requirements={"id" = "\d+"}
     * )
     */
    public function calculation(
        $id,
        EntityManagerInterface $em,
        LoggerInterface $logger
    ): Response
    {
        try {
            /** @var Calculation $calculation */
            $calculation = $em->getRepository(Calculation::class)->find($id);

            if ($calculation === null) {
                return $this->json(
                    sprintf('No calculation has been found for id: %s', $id),
                    Response::HTTP_NOT_FOUND
                );
            } else {
                switch ($calculation->getStatus()) {
                    case Calculation::STATUS_NEW:
                        return $this->json('The calculation is not ready yet.', Response::HTTP_ACCEPTED);
                    case Calculation::STATUS_READY:
                        return $this->json(['result' => $calculation->getOutput()], Response::HTTP_OK);
                    case Calculation::STATUS_ERROR:
                    default:
                        return $this->json('The calculation could not be performed.', Response::HTTP_INTERNAL_SERVER_ERROR);
                }
            }
        } catch (Throwable $e) {
            $logger->error($e, ['Calculation::' . $id]);
            return $this->json(self::CODE_500_MSG, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
