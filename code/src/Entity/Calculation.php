<?php

namespace App\Entity;

use App\Repository\CalculationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CalculationRepository::class)
 */
class Calculation
{
    const STATUS_NEW = 0;
    const STATUS_READY = 1;
    const STATUS_ERROR = -1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $input;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $output;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getInput(): ?array
    {
        return $this->input;
    }

    public function setInput(?array $input): self
    {
        $this->input = $input;

        return $this;
    }

    public function getOutput(): ?float
    {
        return $this->output;
    }

    public function setOutput(?float $output): self
    {
        $this->output = $output;

        return $this;
    }
}
