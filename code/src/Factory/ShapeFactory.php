<?php
namespace App\Factory;

use App\Model\ShapeInterface;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class ShapeFactory
{
    const SHAPE_INDEX = 'shape';
    const DIMENSIONS_INDEX = 'dimensions';

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * ShapeFactory constructor.
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param string|null   $json
     * @return ShapeInterface
     *
     * @throws InvalidArgumentException
     */
    public function crateFromJson(?string $json): ShapeInterface
    {
        if ($json === null || empty($json)) {
            throw new InvalidArgumentException('No data was given.');
        }

        $jsonErrors = $this->validator->validate($json, new Assert\Json());

        if ($jsonErrors->count()) {
            throw new InvalidArgumentException('Malformed JSON was given.');
        }

        return $this->createFromArray(json_decode($json, true));
    }

    /**
     * @param array $data
     * @return ShapeInterface
     */
    public function createFromArray(array $data): ShapeInterface
    {
        if (!isset($data[self::SHAPE_INDEX]) || empty($data[self::SHAPE_INDEX])) {
            throw new InvalidArgumentException('Shape was not specified.');
        }

        $className = 'App\\Model\\' . ucfirst($data[self::SHAPE_INDEX]);

        if (!class_exists($className)) {
            throw new InvalidArgumentException('Unsupported shape was given.');
        }


        if (!isset($data[self::DIMENSIONS_INDEX]) || empty($data[self::DIMENSIONS_INDEX])) {
            throw new InvalidArgumentException('Dimensions were not specified.');
        }


        /** @var ShapeInterface $shape */
        $shape = $className::createFromArray($data[self::DIMENSIONS_INDEX]);
        $errors = $this->validator->validate($shape);

        if ($errors->count()) {
            throw new InvalidArgumentException($this->formatErrors($errors));
        }

        return $shape;
    }

    private function formatErrors(ConstraintViolationListInterface $errors)
    {
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = sprintf("%s: %s", $error->getPropertyPath(), $error->getMessage());
        }

        return implode(' ', $messages);
    }
}
