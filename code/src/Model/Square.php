<?php
namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Square extends AbstractShape
{
    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @Assert\Type(type = {"numeric"})
     */
    protected $a;

    /**
     * Square constructor.
     * @param float|int $a
     */
    public function __construct($a = null)
    {
        $this->a = $a;
    }

    /**
     * @param array $dimensions
     * @return ShapeInterface|self
     */
    public static function createFromArray(array $dimensions): ShapeInterface
    {
        return new self($dimensions['a'] ?? null);
    }

    /**
     * @return float
     */
    public function area(): float
    {
        return pow((float) $this->a, 2);
    }
}