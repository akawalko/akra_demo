<?php
namespace App\Model;

interface ShapeInterface
{
    /**
     * @param array $dimensions
     * @return ShapeInterface
     */
    public static function createFromArray(array $dimensions): ShapeInterface;

    /**
     * @return float
     */
    public function area(): float;
}
