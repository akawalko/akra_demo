<?php
namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Circle extends AbstractShape
{
    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @Assert\Type(type = {"numeric"})
     */
    protected $r;

    /**
     * CircleShape constructor.
     * @param $r
     */
    public function __construct($r = null)
    {
        $this->r = $r;
    }

    /**
     * @param array $dimensions
     * @return ShapeInterface
     */
    public static function createFromArray(array $dimensions): ShapeInterface
    {
        return new self($dimensions['r'] ?? null);
    }

    /**
     * @return float
     */
    public function area(): float
    {
        return pi() * pow((float) $this->r, 2);
    }
}
