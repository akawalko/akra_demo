<?php
namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Rectangle extends AbstractShape
{
    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @Assert\Type(type = {"numeric"})
     */
    protected $a;

    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @Assert\Type(type = {"numeric"})
     */
    protected $b;

    /**
     * Rectangle constructor.
     * @param float|int|string|null     $a
     * @param float|int|string|null     $b
     */
    public function __construct($a = null, $b = null)
    {
        $this->a = $a;
        $this->b = $b;
    }

    /**
     * @param array $dimensions
     * @return ShapeInterface|self
     *
     * @throws \InvalidArgumentException
     */
    public static function createFromArray(array $dimensions): ShapeInterface
    {
        $a = $dimensions['a'] ?? null;
        $b = $dimensions['b'] ?? null;

        return new self($a, $b);
    }

    /**
     * @return float
     */
    public function area(): float
    {
        return (float) $this->a * (float) $this->b;
    }
}
