<?php
namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class EquilateralTriangle extends AbstractShape
{
    /**
     * @Assert\NotBlank
     * @Assert\Positive
     * @Assert\Type(type = {"numeric"})
     */
    protected $a;

    /**
     * EquilateralTriangle constructor.
     * @param float|int|string|null $a
     */
    public function __construct($a = null)
    {
        $this->a = $a;
    }

    /**
     * @param array $dimensions
     * @return ShapeInterface
     */
    public static function createFromArray(array $dimensions): ShapeInterface
    {
        return new self($dimensions['a'] ?? null);
    }

    /**
     * @return float
     */
    public function area(): float
    {
        return pow((float) $this->a, 2) * (sqrt(3) / 4);
    }
}
