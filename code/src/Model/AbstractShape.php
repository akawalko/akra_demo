<?php
namespace App\Model;

use ReflectionClass;
use ReflectionProperty;

abstract class AbstractShape implements ShapeInterface, ExportableInterface
{
    abstract public static function createFromArray(array $dimensions): ShapeInterface;
    abstract public function area(): float;

    public function exportToArray(): array
    {
        $reflect = new ReflectionClass($this);
        $properties = $reflect->getProperties(ReflectionProperty::IS_PROTECTED);
        $dimensions = [];
        foreach ($properties as $property) {
            $dimensions[$property->getName()] = $this->{$property->getName()};
        }

        return [
            'shape' => $reflect->getShortName(),
            'dimensions' => $dimensions
        ];
    }

    public function exportToJson(): string
    {
        return json_encode($this->exportToArray());
    }
}
