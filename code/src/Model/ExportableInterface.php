<?php
namespace App\Model;

interface ExportableInterface
{
    /**
     * @return array
     */
    public function exportToArray(): array;

    /**
     * @return string
     */
    public function exportToJson(): string;
}
