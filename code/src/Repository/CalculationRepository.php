<?php

namespace App\Repository;

use App\Entity\Calculation;
use App\Model\ShapeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Calculation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calculation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calculation[]    findAll()
 * @method Calculation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalculationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Calculation::class);
    }

    public function saveNew(ShapeInterface $shape): Calculation
    {
        $calculation = new Calculation();
        $calculation
            ->setStatus(Calculation::STATUS_NEW)
            ->setInput($shape->exportToArray());


        $this->getEntityManager()->persist($calculation);
        $this->getEntityManager()->flush();
        return $calculation;
    }

    public function update(Calculation $calculation)
    {
        $this->getEntityManager()->persist($calculation);
        $this->getEntityManager()->flush();
    }
}
