# akra_demo

This is simple REST application to calculate the area of several plane figures 
that is utilizing message queue to work asynchronously.

## Installation

    git clone https://gitlab.com/akawalko/akra_demo.git

    cd akra_demo/

    docker-compose build

    docker-compose up -d akra_rabbit && sleep 10 && docker-compose up

The application needs a table in the database to work properly.

Enter the main container and start the migration. 

    docker exec -it  akra_demo_akra_rest_1 sh

    php bin/console doctrine:migrations:migrate

The application is ready to use and to run tests.

## Usage

The application server replies at
    http://localhost:30080


In folder code/docs/ directory there are queries exported from Postman.

[bad_calls.postman_collection.json](code/docs/bad_calls.postman_collection.json)

[good_calls.postman_collection.json](code/docs/good_calls.postman_collection.json)

Or if you a CURL guy like myself, use this for the calculation.

    curl -X POST -H "Content-Type: application/json" -d '{"shape": "rectangle", "dimensions": {"a": 5, "b": 4}}' http://localhost:30080/calculate

You will receive ID for retrieving result of your calculation.

    curl -X GET http://localhost:30080/calculation/{id}

## Tests

In order to run tests, once again enter the main container and run phpunit binary.
    
    docker exec -it  akra_demo_akra_rest_1 sh

    ./vendor/bin/phpunit tests
